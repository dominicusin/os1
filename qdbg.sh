#!/bin/bash

make
if [ $? -ne 0 ] 
then 
    echo Make stage failed
    exit -1
fi
echo Running emulator...
qemu-system-i386 -s -S -cdrom build/os1.iso
