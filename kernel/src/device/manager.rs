use spin::Once;
use alloc::collections::BTreeMap;
use alloc::boxed::Box;
use super::Device;

static MANAGER: Once<DeviceManager> = Once::new();

pub struct DeviceManager {
    devices: BTreeMap<alloc::string::String, Box<Device>>,
}

fn init_manager() -> DeviceManager {
    DeviceManager {
        devices: BTreeMap::new(),
    }
}

