use crate::arch::io::port::outb;

#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ColorCode(u8);

impl ColorCode {
    fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
struct ScreenChar {
    ascii_character: u8,
    color_code: ColorCode,
}

const VGA_WIDTH: usize = 80;
const VGA_HEIGHT: usize = 25;

struct VgaBuffer {
    chars: [ScreenChar; VGA_WIDTH * VGA_HEIGHT],
}

pub struct Writer {
    cursor_position: usize,
    vga_color: ColorCode,
    buffer: &'static mut VgaBuffer,
}

impl Writer {
    pub fn init(&mut self) {
        let vga_color = self.vga_color;
        for y in 0..(VGA_HEIGHT - 1) {
            for x in 0..VGA_WIDTH {
                self.buffer.chars[y * VGA_WIDTH + x] = ScreenChar {
                    ascii_character: b' ',
                    color_code: vga_color,
                }
            }
        }
        self.set_cursor_abs(0);
    }

    fn set_cursor_abs(&mut self, position: usize) {
        unsafe {
            outb(0x3D4, 0x0F);
            outb(0x3D5, (position & 0xFF) as u8);
            outb(0x3D4, 0x0E);
            outb(0x3D4, ((position >> 8) & 0xFF) as u8);
        }
        self.cursor_position = position;
    }

    pub fn set_cursor(&mut self, x: usize, y: usize) {
        self.set_cursor_abs(y * VGA_WIDTH + x);
    }

    pub fn move_cursor(&mut self, offset: usize) {
        self.cursor_position = self.cursor_position + offset;
        self.set_cursor_abs(self.cursor_position);
    }

    pub fn get_x(&mut self) -> u8 {
        (self.cursor_position % VGA_WIDTH) as u8
    }

    pub fn get_y(&mut self) -> u8 {
        (self.cursor_position / VGA_WIDTH) as u8
    }

    pub fn scroll(&mut self) {
        for y in 0..(VGA_HEIGHT - 1) {
            for x in 0..VGA_WIDTH {
                self.buffer.chars[y * VGA_WIDTH + x] = self.buffer.chars[(y + 1) * VGA_WIDTH + x]
            }
        }
        for x in 0..VGA_WIDTH {
            let color_code = self.vga_color;
            self.buffer.chars[(VGA_HEIGHT - 1) * VGA_WIDTH + x] = ScreenChar {
                ascii_character: b' ',
                color_code
            }
        }
    }

    pub fn ln(&mut self) {
        let next_line = self.get_y() as usize + 1;
        if next_line >= VGA_HEIGHT {
            self.scroll();
            self.set_cursor(0, VGA_HEIGHT - 1);
        } else {
            self.set_cursor(0, next_line)
        }
    }

    pub fn write_byte_at_xy(&mut self, byte: u8, color: ColorCode, x: usize, y: usize) {
        self.buffer.chars[y * VGA_WIDTH + x] = ScreenChar {
            ascii_character: byte,
            color_code: color
        }
    }

    pub fn write_byte_at_pos(&mut self, byte: u8, color: ColorCode, position: usize) {
        self.buffer.chars[position] = ScreenChar {
            ascii_character: byte,
            color_code: color
        }
    }

    pub fn write_byte(&mut self, byte: u8) {
        if self.cursor_position >= VGA_WIDTH * VGA_HEIGHT {
            self.scroll();
            self.set_cursor(0, VGA_HEIGHT - 1);
        }
        self.write_byte_at_pos(byte, self.vga_color, self.cursor_position);
        self.move_cursor(1);
    }

    pub fn write_string(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                // printable ASCII byte or newline
                0x20...0xFF => self.write_byte(byte),
                b'\n' => self.ln(),
                // not part of printable ASCII range
                _ => self.write_byte(0xfe),
            }
        }
    }
}

use core::fmt;

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    pub static ref VGA_WRITER : Mutex<Writer> = Mutex::new(Writer {
            cursor_position: 0,
            vga_color: ColorCode::new(Color::LightGray, Color::Black),
            buffer: unsafe { &mut *(0xC00B8000 as *mut VgaBuffer) }
        });
}