pub mod io;
pub mod int;
pub mod idt;
pub mod gdt;
pub mod paging;
pub mod vga;

pub fn arch_init(pd: usize) {
    unsafe {
        vga::VGA_WRITER.lock().init();
        gdt::setup_gdt();
        idt::init_idt();
        paging::setup_pd(pd);
    }
}