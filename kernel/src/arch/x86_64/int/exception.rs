#[no_mangle]
pub extern fn k0_zero_divide() {
    println!("Exception occured - 0 divide");
}

#[no_mangle]
pub extern fn k1_debug() {
    println!("Exception occured - dbg");
}

#[no_mangle]
pub extern fn k2_nmi() {
    println!("Exception occured - non-maskable int");
}

#[no_mangle]
pub extern fn k3_brk_point() {
    println!("Exception occured - brk pnt");
}

#[no_mangle]
pub extern fn k4_overflow() {
    println!("Exception occured overflow");
}

#[no_mangle]
pub extern fn k5_bound_range() {
    println!("Exception occured - bound range");
}

#[no_mangle]
pub extern fn k6_inv_opcode() {
    println!("Exception occured - inv opcode");
}

#[no_mangle]
pub extern fn k7_dev_not_avail() {
    println!("Exception occured - device not avail");
}

#[no_mangle]
pub extern fn k8_double_fault() {
    println!("Exception occured - double fault");
}

#[no_mangle]
pub extern fn kA_inv_tss() {
    println!("Exception occured - inv tss");
}

#[no_mangle]
pub extern fn kB_seg_not_present() {
    println!("Exception occured - segment not present");
}

#[no_mangle]
pub extern fn kC_stack_seg_fault() {
    println!("Exception occured - stack segment fault");
}

#[no_mangle]
pub extern fn kD_protection_fault() {
    println!("Exception occured - protection fault");
}

bitflags! {
    struct PFErrorCode: usize {
        const PROTECTION =      1;      //1 - protection caused, 0 - not present page caused
        const WRITE =           1 << 1; //1 - write caused, 0 - read caused
        const USER_MODE =       1 << 2; //1 - from user mode, 0 - from kernel
        const RESERVED =        1 << 3; //1 - reserved page (PAE/PSE), 0 - not
        const INSTRUCTION =     1 << 4; //1 - instruction fetch caused, 0 - not
    }
}

impl PFErrorCode {
    pub fn to_pd_flags(&self) -> super::super::paging::PDEntryFlags {
        use super::super::paging;
        let mut flags = paging::PDEntryFlags::empty();
        if self.contains(PFErrorCode::WRITE) {
            flags.set(paging::PDEntryFlags::WRITABLE, true);
        }
        if self.contains(PFErrorCode::USER_MODE) {
            flags.set(paging::PDEntryFlags::USER_ACCESSIBLE, true);
        }
        flags
    }
}

#[no_mangle]
pub unsafe extern fn kE_page_fault(ptr: usize, code: usize) {
    use super::super::paging;
    println!("Page fault occured at addr 0x{:X}, code {:X}", ptr, code);
    let phys_address = crate::memory::physical::alloc_page();
    let code_flags: PFErrorCode = PFErrorCode::from_bits(code).unwrap();
    if !code_flags.contains(PFErrorCode::PROTECTION) {
        //page not presented, we need to allocate the new one
        let mut flags: paging::PDEntryFlags = code_flags.to_pd_flags();
        flags.set(paging::PDEntryFlags::HUGE_PAGE, true);
        paging::allocate_page(phys_address, ptr, flags);
        println!("Page frame allocated at Paddr {:#X} Laddr {:#X}", phys_address, ptr);
    } else {
        panic!("Protection error occured, cannot handle yet");
    }
}

#[no_mangle]
pub extern fn k10_x87_ex() {
    println!("Exception occured - floating point");
}

#[no_mangle]
pub extern fn k11_alignment_check() {
    println!("Exception occured - alignment");
}

#[no_mangle]
pub extern fn k12_machine_check() {
    println!("Exception occured - machine");
}

#[no_mangle]
pub extern fn k13_simd_ex() {
    println!("Exception occured - simd");
}

#[no_mangle]
pub extern fn k14_virtualization() {
    println!("Exception occured - virt");
}

#[no_mangle]
pub extern fn k1E_security() {
    println!("Exception occured - security");
}
