global writeb
global writew
global writed
section .text

writeb:
    push ebp
    mov ebp, esp

    mov edx, [ebp + 8] ;port in stack: 8 = 4 (push ebp) + 4 (parameter port length is 2 bytes but stack aligned 4 bytes)
    mov eax, [ebp + 8 + 4] ;value in stack - 8 = see ^, 4 = 1 byte value aligned 4 bytes
    out dx, al ;write byte by port number an dx - value in al

    mov esp, ebp
    pop ebp
    ret

writew:
    push ebp
    mov ebp, esp

    mov edx, [ebp + 8] ;port in stack: 8 = 4 (push ebp) + 4 (parameter port length is 2 bytes but stack aligned 4 bytes)
    mov eax, [ebp + 8 + 4] ;value in stack - 8 = see ^, 4 = 1 word value aligned 4 bytes
    out dx, ax ;write word by port number an dx - value in ax

    mov esp, ebp
    pop ebp
    ret

writed:
    push ebp
    mov ebp, esp

    mov edx, [ebp + 8] ;port in stack: 8 = 4 (push ebp) + 4 (parameter port length is 2 bytes but stack aligned 4 bytes)
    mov eax, [ebp + 8 + 4] ;value in stack - 8 = see ^, 4 = 1 double word value aligned 4 bytes
    out dx, eax ;write double word by port number an dx - value in eax

    mov esp, ebp
    pop ebp
    ret