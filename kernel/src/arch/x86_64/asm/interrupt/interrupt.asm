global int80
extern test_int80
global call_int80

int80:
    pusha
    call test_int80
    popa
    iret

call_int80
    pusha
    int 0x80
    popa
    ret