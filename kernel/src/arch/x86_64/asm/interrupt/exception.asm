global e0_zero_divide
global e1_debug
global e2_nmi
global e3_brk_point
global e4_overflow
global e5_bound_range
global e6_inv_opcode
global e7_dev_not_avail
global e8_double_fault
global eA_inv_tss
global eB_seg_not_present
global eC_stack_seg_fault
global eD_protection_fault
global eE_page_fault
global e10_x87_ex
global e11_alignment_check
global e12_machine_check
global e13_simd_ex
global e14_virtualization
global e1E_security

extern k0_zero_divide
extern k1_debug
extern k2_nmi
extern k3_brk_point
extern k4_overflow
extern k5_bound_range
extern k6_inv_opcode
extern k7_dev_not_avail
extern k8_double_fault
extern kA_inv_tss
extern kB_seg_not_present
extern kC_stack_seg_fault
extern kD_protection_fault
extern kE_page_fault
extern k10_x87_ex
extern k11_alignment_check
extern k12_machine_check
extern k13_simd_ex
extern k14_virtualization
extern k1E_security

section .text

e0_zero_divide:
    pushad
    call k0_zero_divide
    popad
    iret

e1_debug:
    pushad
    call k1_debug
    popad
    iret

e2_nmi:
    pushad
    call k2_nmi
    popad
    iret

e3_brk_point:
    pushad
    call k3_brk_point
    popad
    iret

e4_overflow:
    pushad
    call k4_overflow
    popad
    iret

e5_bound_range:
    pushad
    call k5_bound_range
    popad
    iret

e6_inv_opcode:
    pushad
    call k6_inv_opcode
    popad
    iret

e7_dev_not_avail:
    pushad
    call k7_dev_not_avail
    popad
    iret

e8_double_fault:
    pushad
    call k8_double_fault
    popad
    iret

eA_inv_tss:
    pushad
    call kA_inv_tss
    popad
    iret

eB_seg_not_present:
    pushad
    call kB_seg_not_present
    popad
    iret

eC_stack_seg_fault:
    pushad
    call kC_stack_seg_fault
    popad
    iret

eD_protection_fault:
    pushad
    call kD_protection_fault
    popad
    iret

eE_page_fault:
    pushad
    mov eax, [esp + 32]
    push eax
    mov eax, cr2
    push eax
    call kE_page_fault
    pop eax
    pop eax
    popad
    add esp, 4
    iret

e10_x87_ex:
    pushad
    call k10_x87_ex
    popad
    iret

e11_alignment_check:
    pushad
    call k11_alignment_check
    popad
    iret

e12_machine_check:
    pushad
    call k12_machine_check
    popad
    iret

e13_simd_ex:
    pushad
    call k13_simd_ex
    popad
    iret

e14_virtualization:
    pushad
    call k14_virtualization
    popad
    iret

e1E_security:
    pushad
    call k1E_security
    popad
    iret
