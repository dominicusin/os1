global invalidate_page
section .text

invalidate_page:
    mov   eax, [esp + 4]
    invlpg [eax]
    ret