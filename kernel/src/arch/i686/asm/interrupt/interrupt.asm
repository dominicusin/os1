global int80
extern test_int80
global call_int80
global int_off
global int_on

int_off:
    cli
    ret

int_on:
    sti
    ret

int80:
    pusha
    call test_int80
    popa
    iret

call_int80
    pusha
    int 0x80
    popa
    ret