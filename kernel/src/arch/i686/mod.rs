pub mod io;
pub mod int;
pub mod idt;
pub mod gdt;
pub mod paging;
pub mod vga;
pub mod context;
mod tss;
mod pit;

pub const KERNEL_BASE: usize = 0xC0000000;
pub const MB_INFO_BASE: usize = 0xB0000000;

pub fn get_mb_pointer_base(p_addr: usize) -> usize {
    //if in first 4 MB - map to kernel address space
    if p_addr < 0x400000 {
        KERNEL_BASE
    } else {
        MB_INFO_BASE
    }
}

pub fn arch_init(pd: usize) {
    unsafe {
        vga::VGA_WRITER.lock().init();
        gdt::setup_gdt();
        idt::init_idt();
        paging::setup_pd(pd);
        pit::init();
    }
}