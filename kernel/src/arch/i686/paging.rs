pub const ADDRESS_MASK: u32 = 0xffff_f000;

// #[derive(Debug, Clone, Copy, PartialEq, Eq)]
// pub struct PTableEntry(u32);

// bitflags! {
//     pub struct PTEntryFlags: u32 {
//         const PRESENT =         1;
//         const WRITABLE =        1 << 1;
//         const USER_ACCESSIBLE = 1 << 2;
//         const WRITE_THROUGH =   1 << 3;
//         const NO_CACHE =        1 << 4;
//         const ACCESSED =        1 << 5;
//         const DIRTY =           1 << 6;
//         const GLOBAL =          1 << 8;
//     }
// }

extern {
    fn invalidate_page(page: usize);
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct PDirectoryEntry(u32);

bitflags! {
    pub struct PDEntryFlags: u32 {
        const PRESENT =         1;
        const WRITABLE =        1 << 1;
        const USER_ACCESSIBLE = 1 << 2;
        const WRITE_THROUGH =   1 << 3;
        const NO_CACHE =        1 << 4;
        const ACCESSED =        1 << 5;
        const DIRTY =           1 << 6;
        const HUGE_PAGE =       1 << 7;
        const GLOBAL =          1 << 8;
    }
}

impl PDirectoryEntry {
    pub fn by_phys_address(address: usize, flags: PDEntryFlags) -> Self {
        PDirectoryEntry((address as u32) & ADDRESS_MASK | flags.bits())
    }

    pub fn flags(&self) -> PDEntryFlags {
        PDEntryFlags::from_bits_truncate(self.0)
    }

    pub fn phys_address(&self) -> u32 {
        self.0 & ADDRESS_MASK
    }

    pub fn dbg(&self) -> u32 {
        self.0
    }
}

pub struct PDirectory {
    entries: [PDirectoryEntry; 1024]
}

impl PDirectory {
    pub fn at(&self, idx: usize) -> PDirectoryEntry {
        self.entries[idx]
    }

    pub fn set_by_addr(&mut self, logical_addr: usize, entry: PDirectoryEntry) {
        self.set(PDirectory::to_idx(logical_addr), entry);
    }

    pub fn set(&mut self, idx: usize, entry: PDirectoryEntry) {
        self.entries[idx] = entry;
        unsafe {
            invalidate_page(idx);
        }
    }

    pub fn to_logical_addr(idx: usize) -> usize {
        (idx << 22)
    }

    pub fn to_idx(logical_addr: usize) -> usize {
        (logical_addr >> 22)
    }
}

use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    static ref PAGE_DIRECTORY: Mutex<&'static mut PDirectory> = Mutex::new(
        unsafe { &mut *(0xC0000000 as *mut PDirectory) }
    );
}

pub unsafe fn setup_pd(pd: usize) {
    let mut data = PAGE_DIRECTORY.lock();
    *data = &mut *(pd as *mut PDirectory);
}

pub unsafe fn allocate_page(phys_addr: usize, logical_addr: usize, mut flags: PDEntryFlags) {
    flags.set(PDEntryFlags::PRESENT, true);
    PAGE_DIRECTORY.lock().set_by_addr(logical_addr, PDirectoryEntry::by_phys_address(phys_addr, flags));
}