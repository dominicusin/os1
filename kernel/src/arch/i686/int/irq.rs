use crate::arch::io::port::{outb, inb};

static mut PIT_COUNTS : usize = 0;

#[no_mangle]
pub unsafe extern fn kirq0() {
    PIT_COUNTS = PIT_COUNTS + 1;
    outb(0x20, 0x20);
    if PIT_COUNTS > 20 {
        PIT_COUNTS = 0;
        crate::arch::vga::VGA_WRITER.force_unlock();
        crate::context::switch::do_switch();
    }
}

#[no_mangle]
pub unsafe extern fn kirq1() {
    let ch: char = inb(0x60) as char;
    crate::arch::vga::VGA_WRITER.force_unlock();
    println!("IRQ 1 {}", ch);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq2() {
    println!("IRQ 2");
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq3() {
    println!("IRQ 3");
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq4() {
    println!("IRQ 4");
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq5() {
    println!("IRQ 5");
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq6() {
    println!("IRQ 6");
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq7() {
    println!("IRQ 7");
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq8() {
    println!("IRQ 8");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq9() {
    println!("IRQ 9");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq10() {
    println!("IRQ 10");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq11() {
    println!("IRQ 11");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq12() {
    println!("IRQ 12");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq13() {
    println!("IRQ 13");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq14() {
    println!("IRQ 14");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}

#[no_mangle]
pub unsafe extern fn kirq15() {
    println!("IRQ 15");
    outb(0xA0, 0x20);
    outb(0x20, 0x20);
}
