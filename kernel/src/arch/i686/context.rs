use alloc::boxed::Box;
use super::tss::TSS;
use crate::context::Context;

extern {
    fn arch_switch(current: *mut I686Context, next: *mut I686Context);
    fn arch_start(current: *mut I686Context);
}

#[repr(C, packed)]
pub struct I686Context {
    eax: usize,
    ebx: usize,
    ecx: usize,
    edx: usize,
    esp: usize,
    ebp: usize,
    flags: usize,
    cr3: usize, //page table
}

pub fn switch(_current: *mut Context, _next: *mut Context) {
    unsafe {
        match &(*_next).k_stack {
            Some(st) => {
                let mut k_stack_ptr = st.as_ref().as_ptr() as usize + 1024;
                TSS.esp0 = k_stack_ptr as u32;
                TSS.ss0 = 0x08;
            },
            None => {
                panic!("kernel stack not inited!");
            }
        }
        let current = (*_current).arch as *mut I686Context;
        let next = (*_next).arch as *mut I686Context;
        arch_switch(current, next);
    }
}

pub fn start(_current: *mut Context) {
    println!("Start context");
    unsafe {
        let current_arch = (*_current).arch as *mut I686Context;
        arch_start(current_arch);
    }
}

pub fn set_fn(_ctx: *mut u8) {
    let mut context = unsafe { Box::from_raw(_ctx as *mut I686Context) };
    context.esp -= core::mem::size_of::<usize>();
}

pub fn create_arch_context(context: &Context) -> *mut u8 {
    match &context.stack {
        Some(st) => {
            let stack_ptr = st.as_ref().as_ptr() as usize + 256 * core::mem::size_of::<usize>();
            Box::new(I686Context {
                    eax: 0,
                    ebx: 0,
                    ecx: 0,
                    edx: 0,
                    esp: stack_ptr,
                    ebp: stack_ptr,
                    flags: 0,
                    cr3: 0,
                }).as_ref() as *const I686Context as usize as *mut u8
        },
        None => {
            panic!("Stack not initialized!");
        }
    }
}