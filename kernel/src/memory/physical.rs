//for now maximum phys size is 4GiB
//so to use bitmap for 4MiB pages in 4 states we need 2048 bits or u32 x 64

pub const USABLE: usize = 0;
pub const USED: usize = 1;
pub const RESERVED: usize = 2;
pub const UNUSABLE: usize = 3;

pub const DEAD: usize = 0xDEAD;

struct PhysMemoryInfo {
    pub total: usize,
    used: usize,
    reserved: usize,
    chunks: [u32; 64],
}

impl PhysMemoryInfo {
    // returns (chunk, page)
    pub fn find_free(&self) -> (usize, usize) {
        for chunk in 0..64 {
            for page in 0.. 16 {
                if ((self.chunks[chunk] >> page * 2) & 3) ^ 3 == 3 {
                    return (chunk, page)
                } else {}
            }
        }
        (DEAD, 0)
    }

    // marks page to given flag and returns its address
    pub fn mark(&mut self, chunk: usize, block: usize, flag: usize) -> usize {
        self.chunks[chunk] = self.chunks[chunk] ^ (3 << (block * 2));
        let mask = (0xFFFFFFFC ^ flag).rotate_left(block as u32 * 2);
        self.chunks[chunk] = self.chunks[chunk] & (mask as u32);
        if flag == USED {
            self.used += 1;
        } else if flag == UNUSABLE || flag == RESERVED {
            self.reserved += 1;
        } else {
            if self.used > 0 {
                self.used -= 1;
            }
        }
        (chunk * 16 + block) << 22
    }

    pub fn mark_by_addr(&mut self, addr: usize, flag: usize) {
        let block_num = addr >> 22;
        let chunk: usize = (block_num / 16) as usize;
        let block: usize = block_num - chunk * 16;
        self.mark(chunk, block, flag);
    }

    pub fn count_total(& mut self) {
        let mut count: usize = 0;
        for i in 0..64 {
            let mut chunk = self.chunks[i];
            for _j in 0..16 {
                if chunk & 0b11 != 0b11 {
                    count += 1;
                }
                chunk = chunk >> 2;
            }
        }
        self.total = count;
    }

    pub fn get_total(&self) -> usize {
        self.total
    }

    pub fn get_used(&self) -> usize {
        self.used
    }

    pub fn get_reserved(&self) -> usize {
        self.reserved
    }

    pub fn get_free(&self) -> usize {
        self.total - self.used - self.reserved
    }
}

use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    static ref RAM_INFO: Mutex<PhysMemoryInfo> = 
        Mutex::new(PhysMemoryInfo {
            total: 0,
            used: 0,
            reserved: 0,
            chunks: [0xFFFFFFFF; 64]
        });
}

pub fn init() {
    //the very first page is already used when we have loaded kernek to memory
    let mut ram_info = RAM_INFO.lock();
    ram_info.count_total();
    ram_info.mark(0, 0, USED);
}

pub fn map(addr: usize, len: usize, flag: usize) {
    // if len <= 4MiB then mark whole page with flag
    if len <= 4 * 1024 * 1024 {
        RAM_INFO.lock().mark_by_addr(addr, flag);
    } else {
        let pages: usize = len >> 22;
        for map_page in 0..(pages - 1) {
            map(addr + map_page << 22, 4 * 1024 * 1024, flag);
        }
        map(addr + (pages << 22), len - (pages << 22), flag);
    }
}

pub fn alloc_page() -> usize {
    let mut ram_info = RAM_INFO.lock();
    let (chunk, page) = ram_info.find_free();
    if chunk == DEAD {
        panic!("Out of physical memory");
    } else {
        ram_info.mark(chunk, page, USED)
    }
}

pub fn free_page(addr: usize) {
    let mut ram_info = RAM_INFO.lock();
    ram_info.mark_by_addr(addr, USABLE);
}

//returns pages count
pub fn total() -> usize {
    RAM_INFO.lock().get_total()
}

pub fn used() -> usize {
    RAM_INFO.lock().get_used()
}

pub fn reserved() -> usize {
    RAM_INFO.lock().get_reserved()
}

pub fn free() -> usize {
    RAM_INFO.lock().get_free()
}