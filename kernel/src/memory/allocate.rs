use alloc::alloc::{GlobalAlloc, Layout};

pub struct Os1Allocator;

unsafe impl Sync for Os1Allocator {}

unsafe impl GlobalAlloc for Os1Allocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        use super::logical::{KHEAP_CHUNK_SIZE, allocate_n_chunks};
        let size = layout.size();
        let mut chunk_count: usize = 1;
        if size > KHEAP_CHUNK_SIZE {
            chunk_count = size / KHEAP_CHUNK_SIZE;
            if KHEAP_CHUNK_SIZE * chunk_count != size {
                chunk_count += 1;
            }
        }
        allocate_n_chunks(chunk_count, layout.align())
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        use super::logical::{KHEAP_CHUNK_SIZE, free_chunks};
        let size = layout.size();
        let mut chunk_count: usize = 1;
        if size > KHEAP_CHUNK_SIZE {
            chunk_count = size / KHEAP_CHUNK_SIZE;
            if KHEAP_CHUNK_SIZE * chunk_count != size {
                chunk_count += 1;
            }
        }
        free_chunks(ptr as usize, chunk_count);
    }
}