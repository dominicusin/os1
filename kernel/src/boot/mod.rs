pub mod multiboot2;
pub mod multiboot;

use super::arch;

unsafe fn process_pointer(mb_pointer: usize) -> usize {
    //if in first 4 MB - map to kernel address space
    if mb_pointer < 0x400000 {
        arch::KERNEL_BASE | mb_pointer
    } else {
        arch::paging::allocate_page(mb_pointer, arch::MB_INFO_BASE, 
            arch::paging::PDEntryFlags::PRESENT | 
            arch::paging::PDEntryFlags::WRITABLE | 
            arch::paging::PDEntryFlags::HUGE_PAGE
        );
        arch::MB_INFO_BASE | mb_pointer
    }
}

pub fn init_with_mb1(mb_pointer: usize) {
    let ln_pointer = unsafe { process_pointer(mb_pointer) };
    println!("mb pointer 0x{:X}", ln_pointer);
    let mb_info = multiboot::from_ptr(ln_pointer);
    println!("mb flags: {:?}", mb_info.flags().unwrap());
    if mb_info.flags().unwrap().contains(multiboot::MBInfoFlags::MEM_MAP) {
        multiboot::parse_mmap(mb_info);
        println!("Multiboot memory map parsed, physical memory map has been built");
    } else {
        panic!("MB mmap is not presented");
    }
}