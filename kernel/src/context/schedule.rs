use spin::Mutex;
use alloc::collections::VecDeque;
use super::Context;
use lazy_static::lazy_static;
use alloc::boxed::Box;

lazy_static! {
    static ref CONTEXTS: Mutex<VecDeque<Box<Context>>> = Mutex::new(VecDeque::with_capacity(256));
}

pub fn put(context: &mut Box<Context>) {
    unsafe { 
        let mut contexts = CONTEXTS.lock();
        contexts.push_back(Box::from_raw(context.as_mut())); 
    }
}

pub fn put_ctx(context: Context) {
    let mut contexts = CONTEXTS.lock();
    contexts.push_back(Box::new(context));
}

pub fn pick() -> Option<Box<Context>> {
    let mut contexts = CONTEXTS.try_lock();
    match contexts.as_mut() {
        Some(mut ctxs) => {
            ctxs.pop_front()
        },
        None => {
            None
        }
    }

}