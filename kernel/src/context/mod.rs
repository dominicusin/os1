pub mod schedule;
pub mod switch;

use alloc::boxed::Box;

#[derive(Clone, Copy)]
pub enum ContextState {
    Runnable,
    Blocked,
    Stopped,
    Exited
}

pub struct Context {
    pid: usize,
    ppid: usize,
    state: ContextState,
    pub stack: Option< Box<[usize]> >,
    heap: Option< Box<[u8]> >,
    pub k_stack: Option< Box<[u8]> >,
    pub arch: usize, //pointer!!!
}

impl Context {
    pub fn new (pid: usize, ppid: usize) -> Self {
        let mut ctx = Context {
            pid,
            ppid,
            state: ContextState::Runnable,
            stack: Some(Box::new([0; 256])),
            heap: Some(Box::new([0; 1024])),
            k_stack: Some(Box::new([0; 1024])),
            arch: 0,
        };
        let arch_ctx = crate::arch::context::create_arch_context(&ctx) as usize;
        ctx.arch = arch_ctx;
        ctx
    }

    pub fn set_fn(&mut self, function: fn()) {
        match (self.stack).as_mut() {
            Some(st) => {
                (*st)[255] = function as usize;
                crate::arch::context::set_fn(self.arch as *mut u8);
            },
            None => {
                return;
            }
        }
    }
}