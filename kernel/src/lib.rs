#![no_std]
#![feature(alloc, alloc_error_handler)]
#![feature(asm)]

#[macro_use]
pub mod debug;

#[cfg(target_arch = "x86")]
#[path = "arch/i686/mod.rs"]
pub mod arch;

#[cfg(target_arch = "x86_64")]
#[path = "arch/x86_64/mod.rs"]
pub mod arch;

pub mod boot;
pub mod context;
// pub mod device;
pub mod memory;

#[macro_use]
extern crate bitflags;
extern crate multiboot2;
extern crate alloc;

extern {
    fn int_off();
    fn int_on();
}

#[global_allocator]
static ALLOCATOR: memory::allocate::Os1Allocator = memory::allocate::Os1Allocator;

#[no_mangle]
extern "C" fn kmain(pd: usize, mb_pointer: usize, mb_magic: usize) {
    unsafe{int_off();}
    arch::arch_init(pd);
    match mb_magic {
        0x2BADB002 => {
            println!("multibooted v1, yeah, reading mb info");
            boot::init_with_mb1(mb_pointer);
        },
        0x36d76289 => {
            println!("multiboot v2 is not supported yet");
            panic!("Multiboot2 will be implemented later");
        },
        _ => {
            println!("Kernel should start without multiboot");
            panic!("Self-start will be implemented later");
        },
    }
    memory::init();
    println!("memory: total {} used {} reserved {} free {}", memory::physical::total(), memory::physical::used(), memory::physical::reserved(), memory::physical::free());
    // use alloc::vec::Vec;
    // let mut vec: Vec<usize> = Vec::new();
    // for i in 0..1000000 {
    //     vec.push(i);
    // }
    // println!("vec len {}, ptr is {:?}", vec.len(), vec.as_ptr());
    // println!("Still works, check reusage!");
    // let mut vec2: Vec<usize> = Vec::new();
    // for i in 0..10 {
    //     vec2.push(i);
    // }
    // println!("vec2 len {}, ptr is {:?}, vec is still here? {}", vec2.len(), vec2.as_ptr(), vec.get(1000).unwrap());
    // println!("Still works!");
    // println!("memory: total {} used {} reserved {} free {}", memory::physical::total(), memory::physical::used(), memory::physical::reserved(), memory::physical::free());
    let mut ctx1 = context::Context::new(1, 0);
    ctx1.set_fn(thread1);
    let mut ctx2 = context::Context::new(2, 0);
    ctx2.set_fn(thread2);
    context::schedule::put_ctx(ctx2);
    let mut ctx3 = context::Context::new(3, 0);
    ctx3.set_fn(thread3);
    context::schedule::put_ctx(ctx3);
    let mut ctx4 = context::Context::new(4, 0);
    ctx4.set_fn(thread4);
    context::schedule::put_ctx(ctx4);
    unsafe {int_on();}
    context::switch::do_start(ctx1);
    loop {}
}

pub fn thread1() {
    loop {
        println!("I'm thread 1");
    }
}

pub fn thread2() {
    loop {
        println!("I'm thread 2");
    }
}

pub fn thread3() {
    loop {
        println!("I'm thread 3");
    }
}

pub fn thread4() {
    loop {
        println!("I'm thread 4");
    }
}

use core::panic::PanicInfo;

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    println!("{}", _info);
    loop {}
}

#[alloc_error_handler]
fn oom(_layout: alloc::alloc::Layout) -> ! {
    panic!("seems oom occured");
}
