kernel-clean:
	cargo clean --manifest-path=kernel/Cargo.toml && \
	rm -f build/lib/libkernel.a

KASMS = $(shell find kernel/src/arch/$(ARCH)/asm -name '*.asm')
KOBJS = $(patsubst kernel/src/arch/$(ARCH)/asm/%.asm, $(OBJDIR)/k%-$(ARCH).o, $(KASMS))
KADIRS = $(shell find kernel/src/arch/$(ARCH)/asm -type d -name '*')
KODIRS = $(patsubst kernel/src/arch/$(ARCH)/asm/%, $(OBJDIR)/k%, $(KADIRS))
KRS = $(shell find kernel/src -name '*.rs')

kernel: build/lib/libkernel.a $(KOBJS)

build/lib/libkernel.a: kernel/Cargo.toml $(KRS)
	cargo xbuild -Z unstable-options --manifest-path=kernel/Cargo.toml --target kernel/targets/$(ARCH).json --out-dir=build/lib

$(KODIRS):
	echo $@
	mkdir -p $@

$(KOBJS): $(KASMS) $(KODIRS)
	echo $(KODIRS)
	nasm -felf32 $(patsubst $(OBJDIR)/k%-$(ARCH).o, kernel/src/arch/$(ARCH)/asm/%.asm, $@) -o $@
