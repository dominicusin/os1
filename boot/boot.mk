boot-clean:

boot: build/boot.bin

build/boot.bin: boot/src/arch/${ARCH}/linker.ld build/lib/libkernel.a $(KOBJS) build/link/boot-${ARCH}.o
	ld.lld -flavor ld.lld -T boot/src/arch/${ARCH}/linker.ld -o $@ $^
	objcopy --only-keep-debug $@ $@.sym

build/link/boot-i686.o: boot/src/arch/${ARCH}/boot.asm
	nasm -felf32 $^ -o $@

grub: build/boot.bin
	grub-file --is-x86-multiboot $<

iso: grub
	mkdir -p build/isodir/boot/grub
	cp build/boot.bin build/isodir/boot/boot.bin
	cp isodir/grub.cfg build/isodir/boot/grub/grub.cfg
	grub-mkrescue -o build/os1.iso build/isodir