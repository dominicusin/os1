ARCH ?= i686
OBJDIR = build/link

.PHONY: build

all: iso

include kernel/kernel.mk
include boot/boot.mk

clean: kernel-clean boot-clean
	rm -rf build

